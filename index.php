<?php
// Register Composer generated autoloaders
require __DIR__.'/vendor/autoload.php';

use Ioc\Model\Post,    
    Ioc\Model\Comment,
    Ioc\Service\CommentService;

$post = new Post(
    "A sample post",
    "This is the content of the sample post"
);

$post->attach(new CommentService());

$comment = new Comment(
    "A sample comment",
    "Just commenting on the previous post"
);

$post->setComment($comment);