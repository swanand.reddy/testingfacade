<?php
namespace Ioc\Model;

interface CommentInterface
{
    public function setContent($content);
    public function getContent();
    
    public function setAuthor($author);
    public function getAuthor();
}