<?php
namespace Ioc\Model;

interface PostInterface
{
    public function setTitle($title);
    public function getTitle();
    
    public function setContent($content);
    public function getContent();
    
    public function setComment(CommentInterface $comment);
    public function getComments();
}